# OpenML dataset: munin_4

https://www.openml.org/d/45267

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Munin Bayesian Network. Sample 4.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-massive.html#munin)

- Number of nodes: 1041

- Number of arcs: 1397

- Number of parameters: 80592

- Average Markov blanket size: 3.54

- Average degree: 2.68

- Maximum in-degree: 3

**Authors**: S. Andreassen, F. V. Jensen, S. K. Andersen, B. Falck, U. Kjaerulff, M. Woldbye, A. R. Sorensen, A. Rosenfalck, and F. Jensen.

**Please cite**: ([URL](https://vbn.aau.dk/en/publications/munin-an-expert-emg-assistant)): S. Andreassen, F. V. Jensen, S. K. Andersen, B. Falck, U. Kjaerulff, M. Woldbye, A. R. Sorensen, A. Rosenfalck, and F. Jensen. MUNIN - an Expert EMG Assistant. In Computer-Aided Electromyography and Expert Systems, Chapter 21. Elsevier (Noth-Holland), 1989.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45267) of an [OpenML dataset](https://www.openml.org/d/45267). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45267/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45267/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45267/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

